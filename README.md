# gitlab-ci-include-rules-bug-mwe

This project is a Minimal Working Example for what appears to be a bug in GitLab CI.

The bug appears when using nested `include` directives in combination with `rules` and `needs`.
Specifically, we have a job which is out of a pipeline based on rules, but which declares a `needs` to
_another_ job in an `include`d file, also out of the pipeline (based on the same rules); however,
the web runner declares the configuration invalid because the first job declares an unknown dependency.

## Pipeline structure

The pipeline in this MWE is a simplification of one which my company runs in production:
* On pushes and MR events, we compile and test
* For web runs, the user can specify a PIPELINE_ACTION which is either `deploy` or `teardown`.
  * `deploy` runs build, test, and deploy
  * `teardown` runs do not compile or test

For reasons having to do with sharing a lot of definitions, `deploy` and `teardown` both live in the
same `include`d file.

## Steps to reproduce:

1. Go to Pipelines > Run Pipeline.
2. Enter `teardown` in the `PIPELINE_ACTION` variable.
3. Click the "Run Pipeline" button.

Expected outcome: The pipeline runs. It contains only one job, named `teardown/teardown`.

Actual outcome: The pipeline does not run. The error message displayed says
```
Pipeline cannot be run.
package/build-image job: undefined need: build/build
```
